<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\HyUsuario;


/* @var $this yii\web\View */
/* @var $model app\models\HYArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hyarea-form">

    <?php $form = ActiveForm::begin(); ?>

      
    <?= $form->field($model, 'NOME')->
        dropDownList(ArrayHelper::map(HyUsuario::find()
            ->orderBy('nome')
            ->all(),'ID','NOME'),
            ['prompt' => 'Selecione um usuario']);
    ?>


    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
