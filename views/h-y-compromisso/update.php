<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HYCompromisso */

$this->title = 'Atualizar compromisso: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Compromissos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="hycompromisso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
