<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
 
</div>
<?= Html::a('Nível 1', ['relatorio1'], ['class' => 'btn btn-success']) ?>

<?= Html::a('Nível 2', ['relatorio2'], ['class' => 'btn btn-success']) ?>

<?= Html::a('Nível 3', ['relatorio3'], ['class' => 'btn btn-success']) ?>

<?= Html::a('Nível 4', ['relatorio4'], ['class' => 'btn btn-success']) ?>

<?= Html::a('Nível 5', ['relatorio5'], ['class' => 'btn btn-success']) ?>