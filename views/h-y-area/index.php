<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HYAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Área';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hyarea-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar área', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'NOME',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
