<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HYCompromissoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compromisso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hycompromisso-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar compromisso', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'NOME',
            'HORA',
            'DATA',
            'SITUACAO',
            //'NIVEL',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
