<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hy_usuario".
 *
 * @property int $ID
 * @property string $NOME
 * @property string $EMAIL
 */
class HyUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hy_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME', 'EMAIL'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NOME' => 'Nome',
            'EMAIL' => 'Email',
        ];
    }
}
