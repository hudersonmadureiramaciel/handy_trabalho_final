<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hy_compromisso".
 *
 * @property int $ID
 * @property string $NOME
 * @property string $HORA
 * @property string $DATA
 * @property string $SITUACAO
 * @property string $NIVEL
 */
class HyCompromisso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hy_compromisso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HORA', 'DATA'], 'safe'],
            [['NOME', 'SITUACAO'], 'string', 'max' => 50],
            [['NIVEL'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NOME' => 'Nome',
            'HORA' => 'Hora',
            'DATA' => 'Data',
            'SITUACAO' => 'Situação',
            'NIVEL' => 'Nivel',
        ];
    }
}
