<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HYCompromisso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hycompromisso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NOME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'HORA')->textInput() ?>

    <?= $form->field($model, 'DATA')->textInput() ?>

    <?= $form->field($model, 'SITUACAO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIVEL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
