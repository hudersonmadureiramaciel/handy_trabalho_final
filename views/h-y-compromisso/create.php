<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HYCompromisso */

$this->title = 'Criar Compromisso';
$this->params['breadcrumbs'][] = ['label' => 'Compromissos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hycompromisso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
